# Sample Extension

Malicious-ext which has "geolocation" permissions sends location data to vulnerable-ext which doesn't have permissions

# Testing

Make sure the extensionId variable in malicious-ext/background.js matches the extension ID of the vulnerable-ext after installing.
Clicking on the malcious-ext icon in the browser triggers the data transfer. You can see the data transferred on the background page of vulnerable-ext
